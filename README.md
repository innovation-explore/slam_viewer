# Slam_viewer

**Slam_viewer** is an open source and header only multi-platform library to view the camera trajectory offline.  

<p align="center">
<img src="images/introduction.gif" />
</p>

The link to the main project's page on Github: [https://github.com/salaheddinek/slam_viewer](https://github.com/salaheddinek/slam_viewer).